---
title: AgE 3 Documentation
---

# AgE 3 Documentation

## User documentation

- [Overview](user/overview.md)
- [Console](user/console.md)
- [Configuration properties](user/properties.md)
- [Stream Agents](user/stream-agents/README.md)

## How-tos

- [How to use AgE in your project?](howto/how-to-use-age-in-your-project.md)
- [How to write your own compute module?](howto/how-to-write-your-own-compute-module.md)
- [How to run multiple computations?](howto/how-to-run-multiple-computations.md)

## Contributor documentation

Please, read the following pages if you are extending AgE core or developing a new module to include in
the AgE codebase.

- [How to contribute](contributor/README.md)
- [Architecture](contributor/architecture.md)
