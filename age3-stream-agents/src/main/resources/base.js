/*
 * Copyright (C) 2016 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Base imports and definitions for JS DSL
 **/

var List = Java.type('io.vavr.collection.List');
var HashMap = Java.type('io.vavr.collection.HashMap');
var TimedStopCondition = Java.type('pl.edu.agh.age.compute.stream.TimedStopCondition');
var NoOpLoggingService = Java.type('pl.edu.agh.age.compute.stream.logging.NoOpLoggingService');
var Topology = Java.type('pl.edu.agh.age.compute.api.topology.Topology');
