/*
 * Copyright (C) 2016 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Imports and definitions for EMAS
 */

var Pipeline = Java.type('pl.edu.agh.age.compute.stream.emas.Pipeline');
var EmasAgent = Java.type('pl.edu.agh.age.compute.stream.emas.EmasAgent');
var Solutions = Java.type('pl.edu.agh.age.compute.stream.emas.solution.Solutions');
var Selectors = Java.type('pl.edu.agh.age.compute.stream.emas.Selectors');
var Predicates = Java.type('pl.edu.agh.age.compute.stream.emas.Predicates');
var Generators = Java.type('pl.edu.agh.age.compute.stream.emas.Generators');
var StatisticsKeys = Java.type('pl.edu.agh.age.compute.stream.emas.StatisticsKeys');
