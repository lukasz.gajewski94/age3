Contributors are ordered by the first contribution.

- [Kamil Piętak](https://gitlab.com/kpietak) <kpietak@agh.edu.pl>
- Marek Kisiel-Dorohinicki <doroh@agh.edu.pl>
- [Łukasz Faber](https://gitlab.com/Nnidyu) <faber@agh.edu.pl>
- [Kamil Kuchta](https://gitlab.com/KamilKuchta) <kuchtakamil@gmail.com>
- [Grzegorz Kaczmarczyk](https://gitlab.com/kaczmarczyk) <grzegorz.kaczmarczyk@mail.com> 
- [Andrzej Dymara](https://gitlab.com/dymara) <andrzejdymara@gmail.com>
